﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Battleship
{
    public class ApplicationUser : IUser
    {
        public string Id { get; internal set; }
        public string UserName { get; set; }

        private ApplicationUser(string id, string username)
        {
            this.Id = id;
            this.UserName = username;
        }

        public static ApplicationUser Create(string emailOrUseName)
        {
            var index = emailOrUseName.IndexOf("@");
            if (index >= 0)
            {
                emailOrUseName = emailOrUseName.Substring(0, index);
            }
            return new ApplicationUser(emailOrUseName, emailOrUseName);
        }
    }

    public class ApplicationUserStore : IUserStore<ApplicationUser>
    {
        public Task CreateAsync(ApplicationUser user)
        {
            return Task.FromResult(true);
        }

        public Task<ApplicationUser> FindByIdAsync(string userId)
        {
            return Task.FromResult<ApplicationUser>(ApplicationUser.Create(userId));
        }

        public Task<ApplicationUser> FindByNameAsync(string userName)
        {
            return Task.FromResult<ApplicationUser>(ApplicationUser.Create(userName));
        }

        public Task DeleteAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
        }
    }

    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager() :
            base(new ApplicationUserStore())
        {
        }
    }

    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }
    }
}