﻿///<reference path="../typings/TypeDefinitions.d.ts" />

import Position = require("./Position");

export = Main;

module Main {

    export class Board {
        public element: JQuery;
        public locked = ko.observable<boolean>(false);

        constructor(element: JQuery) {
            this.element = element;

            this._buildBoard();

            this.element.on("mouseover", ".water", $.proxy(this._onMouseOverHandler, this));
            this.element.on("mousedown", ".water", $.proxy(this._onClickHandler, this));

            this.locked.subscribe(isLocked => {
                if (isLocked) {
                    this.clearSelection();
                }
            });
        }

        public addShip(position: string, imageUrl: string): void {
            this.element.find(Position.getCssClass(position)).css('background-image', 'url(' + imageUrl + ')');
        }

        public removeShip(position: string): void {
            this.element.find(Position.getCssClass(position)).css('background-image', "none");
        }

        public hasShip(position: string): boolean {
            var currentImage = this.element.find(Position.getCssClass(position)).css('background-image');
            return !currentImage || currentImage !== "none";
        }

        public hasShot(position: string): boolean {
            return this.element.find(Position.getCssClass(position)).hasClass("shot");
        }

        public addShot(position: string): void {
            this.element.find(Position.getCssClass(position)).addClass("shot");
        }

        public removeShot(position: string): void {
            this.element.find(Position.getCssClass(position)).removeClass("shot");
        }

        public clearShots(): void {
            this.element.find(".water").removeClass("shot");
        }

        public clearShips(): void {
            this.element.find(".water").css('background-image', "none");
        }

        public targetEnemyCell(position: string, $cell?: JQuery): void {
            this._selectCell("selected-enemy", position, $cell);
        }

        public selectCell(position: string, $cell?: JQuery): void {
            this._selectCell("selected", position, $cell);
        }

        public clearSelection(): void {
            this.element.find(".cell").removeClass("selected-enemy selected selected-trail selected-enemy-trail");
        }

        private _selectCell(cssClass: string, position: string, $cell?: JQuery): void {
            $cell = $cell || this.element.find(Position.getCssClass(position));
            var rowClass = ".row" + Position.getRow(position);
            var colClass = ".col" + Position.getCol(position);

            this.clearSelection();
            this.element.find(rowClass).addClass(cssClass + "-trail");
            this.element.find(colClass).addClass(cssClass + "-trail");
            $cell.addClass(cssClass);
        }

        private _onMouseOverHandler(event: JQueryEventObject): void {
            if (!this.locked()) {
                var $cell = $(event.currentTarget);
                var position = $cell.data("data-point");

                this._selectCell("selected", position, $cell);

                $(this).trigger("selected", [position, event]);
            }
        }

        private _onClickHandler(event: JQueryEventObject): void {
            if (!this.locked()) {
                var $cell = $(event.currentTarget);
                var position = $cell.data("data-point");
                $(this).trigger("click", [position, event]);
            }
        }

        private _buildCell(): JQuery {
            return $("<td class='cell'><div class='cell-content'><div class='layer1'></div><div class='layer2'></div></div></td>");
        }

        private _buildBoard() {
            var table = $("<table>", { class: "board" });
            var tbody = $("<tbody>");

            this.element.append(table);
            table.append(tbody);

            // create the water grid
            for (var i = 1; i < 11; i++) {
                var rowClass = "row" + i.toString();
                var row = $("<tr>", { class: rowClass });
                tbody.append(row);

                for (var j = 0; j < 12; j++) {
                    var cell = this._buildCell()
                        .addClass("water")
                        .addClass(rowClass)
                        .addClass("col" + Position.Columns[j].toString())
                        .data("data-point", Position.Columns[j].toString() + i.toString());

                    row.append(cell);
                }
            }

            // Add column headers.
            var row = $("<tr>");
            tbody.prepend(row);
            for (var i = 0; i < 12; i++) {
                var cell = this._buildCell()
                    .addClass("col" + Position.Columns[i].toString())
                    .text(Position.Columns[i]);
                row.append(cell);
            }

            // Add row titles
            tbody.children().each((i, row) => {
                var cell = this._buildCell();
                $(row).prepend(cell);

                if (i > 0) {
                    cell.addClass("row" + i.toString())
                        .text(i.toString());
                }
            });
        }
    }
}