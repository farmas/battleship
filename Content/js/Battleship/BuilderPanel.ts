﻿///<reference path="../typings/TypeDefinitions.d.ts" />

import Panel = require("./Panel");
import Board = require("./Board");
import Position = require("./Position");
import Resources = require("./Resources");

export = Main;

module Main {

    var controls = "<button class='btn btn-primary btn-block' data-bind='enable: allCommitted'>Ready to Play!</button>";
    var content =
        "<div class='builder-panel'>\
            <div class='builder-panel-title'>\
                Select a ship from your fleet and place it on the battle field (right-click to flip it).\
            </div>\
            <div>\
                <ul class='fleet-list' data-bind='foreach: fleet'>\
                    <li data-bind='click: $parent.selectShip, css: { selected: selected(), committed: committed() }'>\
                        <img data-bind='attr: { src: imageUrl }' />\
                        <div>\
                            <span data-bind='text: name' /> (Size <span data-bind='text: size' />)\
                        </div>\
                    </li>\
                </ul>\
            </div>\
        </div>";

    export enum Direction {
        Horizontal,
        Vertical
    }

    export class Ship {
        public size: number;
        public name: string;
        public imageUrl: string;
        public startPosition: string;
        public positions: string[] = [];
        public direction = Direction.Horizontal;
        public committed = ko.observable<boolean>(false);
        public selected = ko.observable<boolean>(false);
        public isValid = false;

        constructor(name: string, size: number, imageUrl: string) {
            this.name = name;
            this.size = size;
            this.imageUrl = imageUrl;
        }

        public toggleDirection(): void {
            if (this.direction === Direction.Vertical) {
                this.direction = Direction.Horizontal;
            } else {
                this.direction = Direction.Vertical;
            }
        }

        public reset(): void {
            this.selected(false);
            this.committed(false);
            this.positions = [];
            this.startPosition = null;
            this.direction = Direction.Horizontal;
        }
    }

    export class BuilderPanel extends Panel.Panel {
        private _board: Board.Board;

        public allCommitted: KnockoutObservable<boolean>;
        public fleet: Ship[] = [
            new Ship("Patrol Boat", 2, Resources.ShipImages.Boat),
            new Ship("Cruiser", 3, Resources.ShipImages.Cruiser),
            new Ship("Destroyer", 4, Resources.ShipImages.Destroyer),
            new Ship("Battleship", 5, Resources.ShipImages.Battleship),
            new Ship("Carrier", 6, Resources.ShipImages.Carrier),
        ];

        constructor(element: JQuery, board: Board.Board) {
            super(element, content, controls);
            this._board = board;

            this.allCommitted = ko.computed(() => {
                return this.fleet.every((ship) => ship.committed());
            });

            ko.applyBindings(this, element[0]);

            element.find(".game-panel-controls button").on("click",() => {
                if (this.allCommitted()) {
                    $(this).trigger("fleetReady");
                }
            });

            this._attachEventHandlers();
        }

        public reset(): void {
            this.fleet.forEach(ship => ship.reset());
        }

        public selectShip = $.proxy((ship: Ship): void => {
            var selectedShip = this._getSelectedShip();
            if (selectedShip) {
                this._removeShip(selectedShip);
                selectedShip.selected(false);
            }

            ship.selected(true);
            ship.committed(false);
        }, this);

        private _getSelectedShip(): Ship {
            return _.find(this.fleet, ship => ship.selected());
        }

        private _onSelectHandler(event: JQueryEventObject, position: string): void {
            var selectedShip = this._getSelectedShip();

            if (selectedShip) {
                selectedShip.startPosition = position;
                this._applyShipPosition(selectedShip);
            }
        }

        private _onClickHandler(event: JQueryEventObject, position: string, clickEvent: JQueryEventObject): void {
            var selectedShip = this._getSelectedShip();

            if (selectedShip && clickEvent) {
                if (clickEvent.which === 1 && selectedShip.isValid) {
                    // left click, if ship is valid commit it.
                    selectedShip.committed(true);
                    selectedShip.selected(false);
                } else if (clickEvent.which === 3) {
                    // right click, change direction of the ship
                    selectedShip.toggleDirection();
                    this._applyShipPosition(selectedShip);
                }
            }
        }

        private _attachEventHandlers(): void {
            this._board.element.on("contextmenu",() => {
                return !this._getSelectedShip();
            });

            $(this._board).on("selected", $.proxy(this._onSelectHandler, this));
            $(this._board).on("click", $.proxy(this._onClickHandler, this));
        }

        private _getNextPoint(point: string, direction: Direction): string {
            return direction === Direction.Horizontal ? Position.goRight(point) : Position.goDown(point);
        }

        private _getFirstImage(direction: Direction): string {
            return direction === Direction.Horizontal ? Resources.TileImages.ShipLeft : Resources.TileImages.ShipUp;
        }

        private _getLastImage(direction: Direction): string {
            return direction === Direction.Horizontal ? Resources.TileImages.ShipRight : Resources.TileImages.ShipDown;
        }

        private _removeShip(ship: Ship): void {
            // remove current cells;
            ship.positions.forEach(position => {
                this._board.removeShip(position);
            });
            this._board.clearShots();
            ship.positions = [];
        }

        private _applyShipPosition(ship: Ship): void {
            this._removeShip(ship);

            // add new cells depending on direction
            ship.isValid = true;
            var position = ship.startPosition;
            for (var i = 0; i < ship.size; i++) {
                if (this._board.hasShip(position)) {
                    // if the cell is occupied, display error and mark ship as error.
                    this._board.addShot(position);
                    ship.isValid = false;
                } else {
                    // the cell is empty, add the ship.
                    if (i === 0) {
                        this._board.addShip(position, this._getFirstImage(ship.direction));
                    } else if (i === ship.size - 1) {
                        this._board.addShip(position, this._getLastImage(ship.direction));
                    } else {
                        this._board.addShip(position, Resources.TileImages.ShipCenter);
                    }

                    ship.positions.push(position);
                }

                position = this._getNextPoint(position, ship.direction);
            }
        }
    }
} 