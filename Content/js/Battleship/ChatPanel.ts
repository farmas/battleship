﻿///<reference path="../typings/TypeDefinitions.d.ts" />

import Panel = require("./Panel");
import Connection = require("./Connection");

export = Main;

module Main {
    var contentTemplate =
        "<div class='chat-panel-title'>Chat</div>\
        <ul class='chat-panel-list' data-bind='foreach: messages'>\
            <li class='chat-panel-entry' data-bind='css: { me: isMe }' tabindex='1'>\
                <div class='chat-panel-user'><span data-bind='text: user'></span>:</div>\
                <div class='chat-panel-message' data-bind='text: message'></div>\
            </li>\
        </ul>";

    var controlsTemplate =
        "<form class='chat-panel-form'>\
            <div class='input-group'>\
                <input type= 'text' class='form-control chat-panel-textbox' placeholder='Message' />\
                <div class='input-group-addon'><i class='fa fa-comment'></i></div>\
            </div>\
        <input class='chat-panel-button' type='submit' tabindex='-1' />\
        </form>";

    export class Message {
        public isMe: boolean;
        constructor(public user: string, public message: string) {
            this.isMe = Connection.Username === user;
        }
    }

    export class ChatPanel extends Panel.Panel {
        private _textbox: JQuery;
        private _list: JQuery;

        public messages = ko.observableArray<Message>([]);

        constructor(element: JQuery) {
            super(element, contentTemplate, controlsTemplate);

            this.setMedium();
            this._textbox = this.element.find(".chat-panel-textbox");
            this._list = this.element.find(".chat-panel-list");

            this.element.find(".chat-panel-form").on("submit",() => {
                var message = this._textbox.val().trim();
                if (message) {
                    this._addMessage(Connection.Username, message);

                    this._textbox.val("");
                    this._textbox.focus();

                    Connection.chat(message);
                }

                return false;
            });

            Connection.onChat.add($.proxy(this._addMessage, this));

            ko.applyBindings(this, this.element[0]);
        }

        public reset(): void {
            this.messages([]);
        }

        private _addMessage(user: string, message: string): void {
            this.messages.push(new Message(user, message));
            this._list.scrollTop(this._list.prop("scrollHeight"));
        }
    }
}