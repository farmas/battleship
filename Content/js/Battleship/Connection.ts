﻿///<reference path="../typings/TypeDefinitions.d.ts" />

export = Main;

module Main {

    var gameHub: any = (<any>$.connection).gameHub;
    var gameHubClient = gameHub.client;
    var gameHubServer = gameHub.server;

    export var Username: string = (<any>window).username;

    export interface Match {
        opponent: string;
        fleet?: string[];
    }

    export interface GameHubUser {
        name: string;
        connectionId: string;
    }

    export var onAddUsers = $.Callbacks();
    export var onRemoveUser = $.Callbacks();
    export var onRequestMatch = $.Callbacks();
    export var onAcceptMatch = $.Callbacks();
    export var onLeaveMatch = $.Callbacks();
    export var onTargetEnemyCell = $.Callbacks();
    export var onShootEnemyCell = $.Callbacks();
    export var onChat = $.Callbacks();

    export function requestMatch(match: Match): JQueryPromise<boolean> {
        return gameHubServer.requestMatch(match);
    }

    export function acceptMatch(match: Match): JQueryPromise<boolean> {
        return gameHubServer.acceptMatch(match);
    }

    export function leaveMatch(): JQueryPromise<any> {
        return gameHubServer.leaveMatch();
    }

    export function targetEnemyCell(position: string): JQueryPromise<boolean> {
        return gameHubServer.targetEnemyCell(position);
    }

    export function shootEnemyCell(position: string): JQueryPromise<boolean> {
        return gameHubServer.shootEnemyCell(position);
    }

    export function endMatch(): void {
        gameHubServer.endMatch();
    }

    export function chat(message: string): void {
        gameHubServer.chat(message);
    }

    gameHubClient.addUsers = (users) => {
        onAddUsers.fire(users);
    };

    gameHubClient.requestMatch = (player, opponent) => {
        onRequestMatch.fire(player, opponent);
    };

    gameHubClient.acceptMatch = (player) => {
        onAcceptMatch.fire(player);
    };

    gameHubClient.leaveMatch = (player) => {
        onLeaveMatch.fire(player);
    };

    gameHubClient.removeUser = (user) => {
        onRemoveUser.fire(user);
    };

    gameHubClient.targetEnemyCell = (position) => {
        onTargetEnemyCell.fire(position);
    }

    gameHubClient.shootEnemyCell = (position) => {
        onShootEnemyCell.fire(position);
    }

    gameHubClient.chat = (user, message) => {
        onChat.fire(user, message);
    }

    $.connection.hub.start();
} 