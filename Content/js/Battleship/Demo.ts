﻿///<reference path="../typings/TypeDefinitions.d.ts" />

import Board = require("./Board");
import Resources = require("./Resources");

export module Main {
    var board = new Board.Board($(".battleship-demo"));

    board.addShip("B2", Resources.TileImages.ShipUp);
    board.addShip("B3", Resources.TileImages.ShipCenter);
    board.addShip("B4", Resources.TileImages.ShipCenter);
    board.addShip("B5", Resources.TileImages.ShipDown);

    board.addShip("E8", Resources.TileImages.ShipLeft);
    board.addShip("F8", Resources.TileImages.ShipCenter);
    board.addShip("G8", Resources.TileImages.ShipCenter);
    board.addShip("H8", Resources.TileImages.ShipCenter);
    board.addShip("I8", Resources.TileImages.ShipCenter);
    board.addShip("J8", Resources.TileImages.ShipRight);

    $(board).on("click",(event, position) => {
        if (!board.hasShot(position)) {
            board.addShot(position);
            console.log("shot", position);
        }
    });
}