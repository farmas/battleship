﻿///<reference path="../typings/TypeDefinitions.d.ts" />

import Panel = require("./Panel");
import Board = require("./Board");
import Connection = require("./Connection");
import ViewModels = require("./ViewModels");

export = Main;

module Main {

    var contentTemplate =
        "<div class='fleet-panel' data-bind='css: { theirTurn: !isMyTurn() }'>\
            <div class='fleet-panel-title'>Their Turn (<span data-bind='text: opponent'></span>)</div>\
            <div class='fleet-panel-board'></div>\
        </div>";

    var controlsTemplate =
        "<div class='fleet-panel-controls'>\
            <div class='fleet-panel-stats'>Hits: <span data-bind='text: theirHits'>00</span></div>\
            <div class='fleet-panel-stats'>Misses: <span data-bind='text: theirMisses'>00</span></div>\
            <div class='fleet-panel-stats'>To Win: <span data-bind='text: toLose'>00</span></div>\
        </div>\
        <div class='fleet-panel-controls'>\
            <button class='btn btn-block fleet-panel-button'>Recall Fleet (start again)</button>\
        </div>";

    export class FleetPanel extends Panel.Panel {
        private _match: ViewModels.Match;
        private _fleetPanelContent: JQuery;
        private _fleetButton: JQuery;
        private _fleetStats: JQuery;

        public board: Board.Board;

        constructor(element: JQuery) {
            super(element, contentTemplate, controlsTemplate);

            this.board = new Board.Board(element.find(".fleet-panel-board"));
            this._fleetPanelContent = this.element.find(".fleet-panel");
            this._fleetButton = this.element.find(".fleet-panel-button");
            this._fleetStats = this.element.find(".fleet-panel-stats");

            this._fleetButton.on("click",() => {
                $(this).trigger("recallFleet");
            });

            this._fleetButton.hide();
            this._fleetStats.hide();
            this._listenToServerEvents();
        }

        public chooseOponent(): void {
            this.board.locked(true);
            this._fleetButton.show();
            this._fleetStats.hide();
        }

        public startMatch(match: ViewModels.Match): void {
            this._match = match;

            this._fleetButton.hide();
            this._fleetStats.show();
            ko.applyBindings(match, this.element[0]);
        }

        public endMatch(): void {
            this._match = null;
            this.board.locked(false);
            this.board.clearShots();
            this.board.clearShips();
            this.board.clearSelection();
            this._fleetPanelContent.removeClass("theirTurn");
            this._fleetButton.hide();
            this._fleetStats.hide();
            ko.cleanNode(this.element[0]);
        }

        private _listenToServerEvents(): void {
            Connection.onLeaveMatch.add($.proxy((player) => {
                if (this._match && this._match.started && this._match.opponent === player) {
                    $(this).trigger("opponentLeft", this._match);
                }
            }, this));

            Connection.onTargetEnemyCell.add(position => {
                this.board.targetEnemyCell(position);
            });

            Connection.onShootEnemyCell.add(position => {
                if (!this._match || this.board.hasShot(position)) {
                    return;
                }

                this.board.addShot(position);

                if (this.board.hasShip(position)) {
                    this._match.theirHits(this._match.theirHits() + 1);
                } else {
                    this._match.theirMisses(this._match.theirMisses() + 1);
                }

                if (this._match.isMatchLost()) {
                    $(this).trigger("matchLost", this._match);
                    this._match = null;
                } else {
                    this._match.isMyTurn(true);
                }
            });
        }
    }
}