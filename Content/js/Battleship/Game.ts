﻿///<reference path="../typings/TypeDefinitions.d.ts" />

import TargettingPanel = require("./TargettingPanel");
import FleetPanel = require("./FleetPanel");
import BuilderPanel = require("./BuilderPanel");
import LobbyPanel = require("./LobbyPanel");
import ChatPanel = require("./ChatPanel");
import ViewModels = require("./ViewModels");
import ModalDialog = require("./ModalDialog");
import Connection = require("./Connection");

export module Main {

    var template =
        "<div class='lobby'></div>\
        <div class='fleet-builder'></div>\
        <div class='board-fleet'></div>\
        <div class='board-shots'></div>\
        <div class='chat'></div>";

    var element = $(".battleship-game");

    element.html(template);

    var fleetPanel = new FleetPanel.FleetPanel(element.find(".board-fleet"));
    var builderPanel = new BuilderPanel.BuilderPanel(element.find(".fleet-builder"), fleetPanel.board);
    var targettingPanel = new TargettingPanel.TargettingPanel(element.find(".board-shots"));
    var lobbyPanel = new LobbyPanel.LobbyPanel(element.find(".lobby"), builderPanel);
    var chatPanel = new ChatPanel.ChatPanel(element.find(".chat"));

    lobbyPanel.fadeLeft();
    targettingPanel.fade();
    chatPanel.fade();

    $(builderPanel).on("fleetReady",(event: JQueryEventObject) => {
        chooseOpponent();
    });

    $(lobbyPanel).on("startMatch",(event: JQueryEventObject, match: ViewModels.Match) => {
        startMatch(match);
    });

    $(fleetPanel).on("matchLost",(event: JQueryEventObject, match: ViewModels.Match) => {
        finishGame("Defeat", match);
    });

    $(fleetPanel).on("opponentLeft",(event: JQueryEventObject, match: ViewModels.Match) => {
        finishGame("Victory (your opponent left)", match);
    });

    $(fleetPanel).on("recallFleet",() => {
        resetGame();
    });

    $(targettingPanel).on("matchWon",(event: JQueryEventObject, match: ViewModels.Match) => {
        finishGame("Victory", match);
    });

    $(targettingPanel).on("leaveMatch",(event: JQueryEventObject, match: ViewModels.Match) => {
        finishGame("Defeat (you left the match)", match);
    });

    $("#fleetReady").on("click",() => {
        chooseOpponent();
    });

    $("#startMatch").on("click",() => {
        startMatch(new ViewModels.Match("foo", ["A1", "A2"]));
    });

    $("#endMatch").on("click",() => {
        finishGame("Dummy", new ViewModels.Match("foo", ["A1", "A2"]));
    });

    $("#resetGame").on("click",() => {
        resetGame();
    });

    function startMatch(match: ViewModels.Match): void {
        match.started = true;
        lobbyPanel.lock();

        fleetPanel.startMatch(match);

        targettingPanel.showLeft();
        targettingPanel.startMatch(match);

        chatPanel.showLeft();
    }

    function chooseOpponent(): void {
        builderPanel.fadeLeft();
        lobbyPanel.lock(false);
        lobbyPanel.fadeLeft(false);
        fleetPanel.showLeft();
        fleetPanel.chooseOponent();
    }

    function resetGame(): void {
        lobbyPanel.fadeLeft();
        lobbyPanel.endMatch();

        builderPanel.fadeLeft(false);
        builderPanel.reset();

        fleetPanel.endMatch();
        fleetPanel.showLeft(false);

        targettingPanel.endMatch();
        targettingPanel.showLeft(false);

        chatPanel.showLeft(false);
        chatPanel.reset();
    }

    function finishGame(dialogTitle: string, match: ViewModels.Match): void {
        Connection.endMatch();
        ModalDialog.show({
            title: dialogTitle,
            message: "Your score: " + match.myHits() + ". Their score: " + match.theirHits(),
            buttonText: "Play Again",
            onHide: resetGame
        });
    }

    (<any>ko.utils.domNodeDisposal).cleanExternalData = function () {
        // Do nothing. Now any jQuery data associated with elements will
        // not be cleaned up when the elements are removed from the DOM.
    };
}