﻿///<reference path="../typings/TypeDefinitions.d.ts" />

import Panel = require("./Panel");
import Connection = require("./Connection");
import ViewModels = require("./ViewModels");
import ModalDialog = require("./ModalDialog");
import BuilderPanel = require("./BuilderPanel");

export = Main;

module Main {

    var contentTemplate =
        "<div class='lobby-panel-title'>Lobby</div>\
        <ul class='lobby-panel-list' data-bind='foreach: users'>\
            <li class='lobby-panel-user' data-bind='click: $parent.selectUser, attr: { title: tooltip }, css: { disabled: !enabled(), selected: isSelected }'>\
                <i class='fa' data-bind='css: { \"fa-flag\": isWaitingForYou, \"fa-clock-o\": isWaitingForThem }'></i>\
                <span data-bind='text: name'></span>\
            </li>\
        </ul>";

    var controlsTemplate =
        "<button class='btn btn-block' data-bind='enable: buttonEnabled, css: { \"btn-primary\": buttonEnabled() }'>\
            <span data-bind='text: buttonMessage'></span>\
        </button>";

    export enum Status {
        Idle = 1,
        WaitingForThem = 2,
        WaitingForYou = 3,
        Playing = 4
    }

    export class User {
        public name: string;
        public id: string;
        public status = ko.observable<Status>(Status.Idle);
        public isSelected = ko.observable<boolean>(false);
        public enabled: KnockoutComputed<boolean>;
        public tooltip: KnockoutComputed<string>;
        public isWaitingForThem: KnockoutComputed<boolean>;
        public isWaitingForYou: KnockoutComputed<boolean>;

        public reset(): void {
            this.status(Status.Idle);
            this.isSelected(false)
        }

        constructor(id: string, name: string) {
            this.id = id;
            this.name = name;

            this.enabled = ko.computed<boolean>(() => {
                return Connection.Username !== this.name
                    && (this.status() === Status.Idle || this.status() === Status.WaitingForYou);
            });

            this.isWaitingForThem = ko.computed<boolean>(() => {
                return this.status() === Status.WaitingForThem;
            });

            this.isWaitingForYou = ko.computed<boolean>(() => {
                return this.status() === Status.WaitingForYou;
            });

            this.tooltip = ko.computed<string>(() => {
                if (this.name === Connection.Username) {
                    return "This is you.";
                }

                switch (this.status()) {
                    case Status.Idle:
                        return "Challenge this user to a match.";
                    case Status.Playing:
                        return "This user is already playing a match.";
                    case Status.WaitingForYou:
                        return "This user is waiting to play with you.";
                    case Status.WaitingForThem:
                        return "You are waiting to play with this user.";
                }
            });
        }
    }

    export class LobbyPanel extends Panel.Panel {
        public users: KnockoutObservableArray<User>;
        public buttonMessage: KnockoutComputed<string>;
        public buttonEnabled: KnockoutComputed<boolean>;
        public selectUser: Function;

        private _match: ViewModels.Match;
        private _builder: BuilderPanel.BuilderPanel;

        constructor(element: JQuery, builder: BuilderPanel.BuilderPanel) {
            super(element, contentTemplate, controlsTemplate);

            this.setSmall();

            this.users = ko.observableArray([]);
            this._builder = builder;

            this.buttonMessage = ko.computed<string>(() => {
                switch (this._getSelectedUserStatus()) {
                    case Status.WaitingForYou:
                        return "Accept Match";
                    case Status.Playing:
                        return "User Playing";
                    default:
                        return "Request Match";
                }
            });

            this.buttonEnabled = ko.computed<boolean>(() => {
                var selectedUser = this.getSelectedUser();
                return selectedUser && selectedUser.enabled();
            });

            this.selectUser = (user: User) => {
                if (user.enabled()) {
                    this.users().forEach(user => user.isSelected(false));
                    user.isSelected(true);
                }
            };

            ko.applyBindings(this, element[0]);

            this.element.find(".game-panel-controls button").on("click",() => {
                var selectedUser = this.getSelectedUser();

                if (selectedUser && selectedUser.status() === Status.Idle) {
                    this._requestMatch(selectedUser);
                } else if (selectedUser && selectedUser.status() === Status.WaitingForYou) {
                    this._acceptMatch(selectedUser);
                }
            });

            this._listenToServerEvents();
        }

        public getSelectedUser(): User {
            return _.find(this.users(), user => user.isSelected());
        }

        public endMatch(): void {
            var selectedUser = this.getSelectedUser();
            if (selectedUser) {
                selectedUser.reset();
            }

            this._match = null;
        }

        private _abortMatch(opponent: User): void {
            Connection.leaveMatch();
            this._match = null;
            opponent.status(Status.Idle);
            opponent.isSelected(false);
        }

        private _getFleet(): string[] {
            return _.flatten(this._builder.fleet.map(ship => ship.positions));
        }

        private _acceptMatch(selectedUser: User): void {
            this._match = new ViewModels.Match(selectedUser.name, this._getFleet());

            Connection.acceptMatch(this._match).done(result => {
                if (result && this._match && !this._match.started && this._match.opponent === selectedUser.name) {
                    selectedUser.status(Status.Playing);
                    this._match.isMyTurn(true);
                    $(this).trigger("startMatch", this._match);
                } else {
                    this._abortMatch(selectedUser);
                }
            });
        }

        private _requestMatch(selectedUser: User): void {
            this._match = new ViewModels.Match(selectedUser.name, this._getFleet());

            Connection.requestMatch(this._match).done(result => {
                if (!result) {
                    this._abortMatch(selectedUser);
                } else {
                    selectedUser.status(Status.WaitingForThem);
                    ModalDialog.show({
                        title: "Waiting for player",
                        message: "Please wait for the player to accept your challenge.",
                        buttonText: "Cancel",
                        onHide: () => {
                            if (this._match && !this._match.started && selectedUser && this._match.opponent === selectedUser.name) {
                                this._abortMatch(selectedUser);
                            }
                        }
                    });
                }
            });
        }

        private _listenToServerEvents(): void {
            Connection.onAddUsers.add((users: Connection.GameHubUser[]) => {
                this.users.push.apply(this.users, users.map(user => new User(user.connectionId, user.name)));
            });

            Connection.onRemoveUser.add((userToRemove: string) => {
                var index = _.findIndex(this.users(), user => user.name === userToRemove);

                if (index >= 0) {
                    this.users.splice(index, 1);
                }
            });

            Connection.onRequestMatch.add((player: string, opponent: string) => {
                var user = _.find(this.users(), user => user.name === player);

                if (this._match && this._match.opponent === player) {
                    // the opponent that I was waiting for requested another match. Abort mine.
                    this._abortMatch(user);
                }

                if (opponent === Connection.Username) {
                    user.status(Status.WaitingForYou);
                } else {
                    user.status(Status.Playing);
                }
            });

            Connection.onAcceptMatch.add((player: string) => {
                if (this._match && !this._match.started && this._match.opponent === player) {
                    var user = _.find(this.users(), user => user.name === player);
                    user.status(Status.Playing);

                    $(this).trigger("startMatch", this._match);
                    ModalDialog.hide();
                }
            });

            Connection.onLeaveMatch.add((player: string) => {
                var user = _.find(this.users(), user => user.name === player);

                if (this._match && this._match.started && this._match.opponent === player) {
                    // todo: end the game.
                }

                user.status(Status.Idle);
            });
        }

        private _getSelectedUserStatus(): Status {
            var selectedUser = this.getSelectedUser();
            return selectedUser && selectedUser.status();
        }
    }
} 