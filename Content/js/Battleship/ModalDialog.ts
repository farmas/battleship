﻿///<reference path="../typings/TypeDefinitions.d.ts" />

export = Main;

module Main {
    var $modal: any;
    var viewModel: ViewModel;
    var template =
        "<div class='battleship-modal modal fade'>\
            <div class='modal-dialog'>\
                <div class='modal-content'>\
                    <div class='modal-header'>\
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>\
                        <h4 class='modal-title' data-bind='text: title'></h4>\
                    </div>\
                    <div class='modal-body'>\
                        <p data-bind='text: message'></p>\
                    </div>\
                    <div class='modal-footer'>\
                        <button type='button' class='btn btn-default' data-bind='text: buttonText, click: clickButton'></button>\
                    </div>\
                </div>\
            </div>\
        </div>";

    $("body").append(template);

    export interface Options {
        title: string;
        message: string;
        buttonText?: string;
        onButtonClicked?: () => void;
        onHide?: () => void;
    }

    class ViewModel {
        public title = ko.observable<string>();
        public message = ko.observable<string>();
        public buttonText = ko.observable<string>();
        public buttonClicked = false;
        public clickButton: Function;

        public constructor() {
            this.clickButton = () => {
                this.buttonClicked = true;
            };
        }
    }

    export function hide(): void {
        $modal.modal("hide");
    }

    export function show(options: Options): void {
        if (!$modal) {
            $modal = $(".battleship-modal");
            viewModel = new ViewModel();
            ko.applyBindings(viewModel, $modal[0]);
        }

        viewModel.title(options.title);
        viewModel.message(options.message);
        viewModel.buttonText(options.buttonText || "Ok");
        viewModel.buttonClicked = false;

        $modal.on("hide.bs.modal",() => {
            options && options.onHide && options.onHide();
            $modal.off("hide.bs.modal");
        });

        $modal.find(".btn").on("click",() => {
            $modal.modal("hide");
        });

        $modal.modal("show");
    }
}