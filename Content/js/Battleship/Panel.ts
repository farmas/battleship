﻿///<reference path="../typings/TypeDefinitions.d.ts" />

export = Main;

module Main {
    var template =
        "<div class='game-panel'>\
            <div class='game-panel-content'></div>\
            <div class='game-panel-controls'></div>\
            <div class='game-panel-shield'></div>\
        </div>";

    export class Panel {
        private _wrapper: JQuery;
        private _content: JQuery;
        private _controls: JQuery;

        public element: JQuery;

        constructor(element: JQuery, contentHtml: string, controlsHtml: string = null) {
            this.element = element;
            this.element.html(template);
            this._wrapper = this.element.find(".game-panel");
            this._content = this.element.find(".game-panel-content");
            this._controls = this.element.find(".game-panel-controls");

            this.element.addClass("game-panel-wrapper");
            this.contentElement.html(contentHtml);

            if (controlsHtml) {
                this.controlsElement.html(controlsHtml);
            }
        }

        public showRight(on: boolean = true): void {
            this._wrapper.toggleClass("show-right", on);
        }

        public showLeft(on: boolean = true): void {
            this._wrapper.toggleClass("show-left", on);
        }

        public fadeRight(on: boolean = true): void {
            this._wrapper.toggleClass("fade-right", on);
        }

        public fadeLeft(on: boolean = true): void {
            this._wrapper.toggleClass("fade-left", on);
        }

        public fade(on: boolean = true): void {
            this._wrapper.toggleClass("fade", on);
        }

        public get contentElement(): JQuery {
            return this._content;
        }

        public get controlsElement(): JQuery {
            return this._controls;
        }

        public setSmall(on: boolean = true): void {
            this._wrapper.toggleClass("game-panel-small", on);
        }

        public setMedium(on: boolean = true): void {
            this._wrapper.toggleClass("game-panel-medium", on);
        }

        public lock(on: boolean = true): void {
            this._wrapper.toggleClass("locked", on);
        }
    }
} 