﻿///<reference path="../typings/TypeDefinitions.d.ts" />

export = Main;

module Main {

    export var Columns = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"];

    export function getCssClass(point: string): string {
        return ".col" + point[0] + ".row" + point.substr(1);
    }

    export function getCol(point: string): string {
        return point[0];
    }

    export function getRow(point: string): string {
        return point.substr(1);
    }

    export function goLeft(point: string): string {
        var col = getCol(point);
        var newCol = Columns.indexOf(col) - 1;

        if (newCol < 0) {
            return point;
        } else {
            return Columns[newCol] + getRow(point);
        }
    }

    export function goRight(point: string): string {
        var col = getCol(point);
        var newCol = Columns.indexOf(col) + 1;

        if (newCol >= Columns.length) {
            return point;
        } else {
            return Columns[newCol] + getRow(point);
        }
    }

    export function goUp(point: string): string {
        var newRow = parseInt(getRow(point)) - 1;

        if (newRow < 1) {
            return point;
        } else {
            return getCol(point) + newRow.toString();
        }
    }

    export function goDown(point: string): string {
        var newRow = parseInt(getRow(point)) + 1;

        if (newRow > 10) {
            return point;
        } else {
            return getCol(point) + newRow.toString();
        }
    }
} 