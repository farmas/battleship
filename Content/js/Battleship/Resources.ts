﻿///<reference path="../typings/TypeDefinitions.d.ts" />

export module TileImages {
    export var ShipCenter = "Content/images/ship-center.png";
    export var ShipUp = "Content/images/ship-up.png";
    export var ShipDown = "Content/images/ship-down.png";
    export var ShipLeft = "Content/images/ship-left.png";
    export var ShipRight = "Content/images/ship-right.png";
}

export module ShipImages {
    export var Battleship = "Content/images/ship-battleship.png";
    export var Boat = "Content/images/ship-boat.png";
    export var Carrier = "Content/images/ship-carrier.png";
    export var Cruiser = "Content/images/ship-cruiser.png";
    export var Destroyer = "Content/images/ship-destroyer.png";
}