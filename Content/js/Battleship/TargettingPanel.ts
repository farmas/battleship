﻿///<reference path="../typings/TypeDefinitions.d.ts" />

import Panel = require("./Panel");
import Board = require("./Board");
import Connection = require("./Connection");
import Resources = require("./Resources");
import ViewModels = require("./ViewModels");

export = Main;

module Main {

    var contentTemplate =
        "<div class='targetting-panel' data-bind='css: { myTurn: isMyTurn }'>\
            <div class='targetting-panel-title'>Your Turn</div>\
            <div class='targetting-panel-board'></div>\
        </div>";

    var controlsTemplate =
        "<div class='targetting-panel-controls'>\
            <div class='targetting-panel-stats'>Hits: <span data-bind='text: myHits'>00</span></div>\
            <div class='targetting-panel-stats'>Misses: <span data-bind='text: myMisses'>00</span></div>\
            <div class='targetting-panel-stats'>To Win: <span data-bind='text: toWin'>00</span></div>\
        </div>\
        <div class='targetting-panel-controls'>\
            <button class='btn btn-block'>I give up</button>\
        </div>";

    export class TargettingPanel extends Panel.Panel {
        private _myTurnSubscription: KnockoutSubscription;
        private _match: ViewModels.Match;
        private _enabled = ko.observable<boolean>(false);

        public board: Board.Board;

        constructor(element: JQuery) {
            super(element, contentTemplate, controlsTemplate);

            this.board = new Board.Board(element.find(".targetting-panel-board"));

            $(this.board).on("selected", $.proxy(this._onSelectHandler, this));
            $(this.board).on("click", $.proxy(this._onClickHandler, this));
            this.controlsElement.find("button").on("click",() => {
                if (this._match) {
                    Connection.leaveMatch();
                    $(this).trigger("leaveMatch", this._match);
                }
            });
        }

        public startMatch(match: ViewModels.Match): void {
            this._match = match;
            ko.applyBindings(match, this.element[0]);

            this._myTurnSubscription = match.isMyTurn.subscribe($.proxy(this._turnChangedHandler, this));
            this._turnChangedHandler(match.isMyTurn());
        }

        public endMatch(): void {
            this._match = null;
            this._myTurnSubscription.dispose();
            this._enabled(false);
            this.board.clearShips();
            this.board.clearSelection();
            this.board.clearShots();
            ko.cleanNode(this.element[0]);
        }

        private _turnChangedHandler(myTurn: boolean): void {
            this._enabled(myTurn);
        }

        private _onSelectHandler(event: JQueryEventObject, position: string): void {
            Connection.targetEnemyCell(position);
        }

        private _onClickHandler(event: JQueryEventObject, position: string): void {
            if (this._enabled()) {
                Connection.shootEnemyCell(position).done(result => {
                    if (!this._match || this.board.hasShot(position)) {
                        return;
                    }
                    this.board.addShot(position);

                    if (result) {
                        this._match.myHits(this._match.myHits() + 1);
                        this.board.addShip(position, Resources.TileImages.ShipCenter);
                    } else {
                        this._match.myMisses(this._match.myMisses() + 1);
                    }

                    if (this._match.isMatchWon()) {
                        $(this).trigger("matchWon", this._match);
                        this._match = null;
                    } else {
                        this._match.isMyTurn(false);
                    }
                });
            }
        }
    }
} 