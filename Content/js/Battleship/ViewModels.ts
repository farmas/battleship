﻿///<reference path="../typings/TypeDefinitions.d.ts" />

export = Main;

module Main {
    export class Match {
        public opponent: string;
        public myHits = ko.observable<number>(0);
        public theirHits = ko.observable<number>(0);
        public myMisses = ko.observable<number>(0);
        public theirMisses = ko.observable<number>(0);
        public started = false;
        public isMyTurn = ko.observable<boolean>(false);
        public fleet: string[] = [];
        public toWin: KnockoutComputed<number>;
        public toLose: KnockoutComputed<number>;

        public constructor(opponent: string, fleet: string[]) {
            this.opponent = opponent;
            this.fleet = fleet;
            this.toWin = ko.computed<number>(() => {
                return this.fleet.length - this.myHits();
            });

            this.toLose = ko.computed<number>(() => {
                return this.fleet.length - this.theirHits();
            });
        }

        public isMatchWon(): boolean {
            return this.myHits() === this.fleet.length;
        }

        public isMatchLost(): boolean {
            return this.theirHits() === this.fleet.length;
        }
    }
}