﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Battleship.Controllers
{
    public class AuthController : Controller
    {
        private class AuthChallengeActionResult : HttpUnauthorizedResult
        {
            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }

            public AuthChallengeActionResult(string provider, string redirectUri)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
            }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            var authenticationManager = HttpContext.GetOwinContext().Authentication;
            authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Game");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider)
        {
            return new AuthChallengeActionResult(provider, Url.Action("ExternalLoginCallback"));
        }

        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback()
        {
            var authenticationManager = HttpContext.GetOwinContext().Authentication;
            var signInManager = HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            var loginInfo = await authenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo != null)
            {
                await signInManager.SignInAsync(ApplicationUser.Create(loginInfo.Email), true, false);
            }

            return RedirectToAction("Index", "Game");
        }
    }
}