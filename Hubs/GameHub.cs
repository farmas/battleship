﻿using Battleship.Models;
using Microsoft.AspNet.SignalR;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

namespace Battleship.Hubs
{
    [Authorize]
    public class GameHub : Hub
    {
        private readonly static ConcurrentDictionary<string, string> _connections = new ConcurrentDictionary<string, string>();
        private readonly static ConcurrentDictionary<string, Match> _matches = new ConcurrentDictionary<string, Match>();

        public void Chat(string message)
        {
            string opponentId;
            if (TryGetOpponentId(out opponentId))
            {
                Clients.Client(opponentId).chat(this.Context.User.Identity.Name, message);
            }
        }

        public void EndMatch()
        {
            Match match;
            _matches.TryRemove(this.Context.User.Identity.Name, out match);
        }

        public bool ShootEnemyCell(string position)
        {
            string opponentId;
            Match opponentMatch;
            if (TryGetOpponentMatch(out opponentId, out opponentMatch))
            {
                Clients.Client(opponentId).shootEnemyCell(position);

                return opponentMatch.Fleet.Contains(position);
            }

            return false;
        }

        public bool TargetEnemyCell(string position)
        {
            string opponentId;
            if (TryGetOpponentId(out opponentId))
            {
                Clients.Client(opponentId).targetEnemyCell(position);
                return true;
            }

            return false;
        }

        public bool RequestMatch(Match match)
        {
            var username = this.Context.User.Identity.Name;
            Match opponentMatch;
            var opponentFree = !_matches.TryGetValue(match.Opponent, out opponentMatch);

            if (opponentFree && _matches.TryAdd(username, match))
            {
                Clients.Others.requestMatch(username, match.Opponent);
            }

            return opponentFree;
        }

        public bool AcceptMatch(Match match)
        {
            var username = this.Context.User.Identity.Name;
            Match opponentMatch;

            if (_matches.TryGetValue(match.Opponent, out opponentMatch)
                && opponentMatch.Opponent == username
                && _matches.TryAdd(username, match))
            {
                Clients.Others.acceptMatch(username);
                return true;
            }

            return false;
        }

        public void LeaveMatch()
        {
            var username = this.Context.User.Identity.Name;
            Match match;
            _matches.TryRemove(username, out match);

            Clients.Others.leaveMatch(username);
        }

        public override Task OnConnected()
        {
            var username = this.Context.User.Identity.Name;
            Match match;
            _connections.AddOrUpdate(username, Context.ConnectionId, (key, val) => Context.ConnectionId);
            _matches.TryRemove(username, out match);

            // Give the new client all the users.
            Clients.Caller.addUsers(_connections.Select(keyVal =>
            {
                return new
                {
                    name = keyVal.Key,
                    connectionId = keyVal.Value
                };
            }));

            // Give old clients the new user.
            Clients.Others.addUsers(Enumerable.Range(1, 1).Select(i => new
            {
                name = username,
                connectionId = Context.ConnectionId
            }));

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var username = this.Context.User.Identity.Name;
            string val;
            Match match;
            _connections.TryRemove(username, out val);
            _matches.TryRemove(username, out match);

            Clients.Others.removeUser(username);
            return base.OnDisconnected(stopCalled);
        }

        private bool TryGetOpponentId(out string connectionId)
        {
            string opponentName;
            Match opponentMatch;
            return TryGetOpponent(out opponentName, out connectionId, out opponentMatch);
        }

        private bool TryGetOpponentMatch(out string connectionId, out Match opponentMatch)
        {
            string opponentName;
            return TryGetOpponent(out opponentName, out connectionId, out opponentMatch);
        }

        private bool TryGetOpponent(out string opponentName, out string opponentId, out Match opponentMatch)
        {
            var username = this.Context.User.Identity.Name;
            Match myMatch;
            opponentMatch = null;
            opponentId = null;
            opponentName = null;

            var result = _matches.TryGetValue(username, out myMatch)
                && _connections.TryGetValue(myMatch.Opponent, out opponentId)
                && _matches.TryGetValue(myMatch.Opponent, out opponentMatch);

            if (result)
            {
                opponentName = myMatch.Opponent;
            }

            return result;
        }
    }
}