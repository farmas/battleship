﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Battleship.Models
{
    public class Match
    {
        public string Opponent { get; set; }
        public string[] Fleet { get; set; }
    }
}