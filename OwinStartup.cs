﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

[assembly: OwinStartupAttribute(typeof(Battleship.OwinStartup))]
namespace Battleship
{
    public class OwinStartup
    {
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext<ApplicationUserManager>((options, context) => new ApplicationUserManager());
            app.CreatePerOwinContext<ApplicationSignInManager>((options, context) =>
            {
                return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
            });

            app.UseKentorOwinCookieSaver();
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/"),
            });
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            var appSettings = WebConfigurationManager.AppSettings;
            var authOptions = new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = appSettings["GoogleClientId"],
                ClientSecret = appSettings["GoogleClientSecret"],
            };

            authOptions.Scope.Clear();
            authOptions.Scope.Add("email");

            app.UseGoogleAuthentication(authOptions);
            app.MapSignalR();
        }
    }
}