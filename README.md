
A classic game of Battleship built with SignalR to support concurrent users playing the game. This is a personal project used to learn SignalR and Owin.

### Technologies ###

* Typescript
* Owin external authentication (Google provider).
* SignalR
* KnockoutJS

### Screen shot ###

![Battleship Sample](https://www.dropbox.com/s/75p9nicqa23whib/battleship-sample.png?raw=1 "Battleship Sample")